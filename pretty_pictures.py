from gasp import *

begin_graphics()

def draw_face(x, y):
    Circle((x,y),40)
    Circle((x-20,y+10),5)
    Circle((x+20,y+10),5)
    Arc((x,y),30,y+20,x+20)
    Line((x,y+10),(x,y-10))

draw_face(300,200)
draw_face(400,200)
draw_face(350,270)

update_when('key_pressed')      # you know what this does by now...
end_graphics()
